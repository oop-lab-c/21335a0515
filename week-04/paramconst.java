import java.lang.*;
import java.util.Scanner;

class paramconst {

  paramconst() {
    System.out.println(
      "\tMaharaj Vijayaram Gajapathi Raj college of Engineering\n"
    );
    System.out.println("\t\tCollege code is:33\n");
  }

  paramconst(String name, double n) {
    System.out.println("Your name is: " + name);
    System.out.println("\nyour sem score is: " + n);
  }

  public static void main(String[] args) {
    System.out.println("enter your name:");
    Scanner input = new Scanner(System.in);
    String name = input.nextLine();

    System.out.println("enter your sem percentage:");
    double n = input.nextDouble();

    // WK obj;
    paramconst s = new paramconst();
    paramconst k = new paramconst(name, n);
  }

  protected void finalize() {
    System.out.println("Object is destroyed by the Garbage Collector");
  }
}
