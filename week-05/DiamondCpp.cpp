#include<iostream>
using namespace std;
class person
{
    public:
    person()
    {
        cout<<"call to mom"<<endl;
    }
};
class father:virtual public person
{
    public:
    father()
    {
        cout<<"call to dad"<<endl;
    }
};
class mother: virtual public person
{
    public:
    mother()
    {
        cout<<"call to bro"<<endl;
    }
};
class child:public father,public mother
{
    public:
    child()
    {
        cout<<"call sucessful"<<endl;
    }
};
int main()
{
    child obj;
    return 0;
}
