 class Bird
{
    void fly()
    {
        System.out.println("I can fly in the sky");
    }
}
class parrot extends Bird
{
    void eat()
    {
        System.out.println("I eat apple");
    }
}
class sparrot extends parrot // multiple inheitance 
{
    public static void main(String[] args)
    {
        sparrot p = new sparrot();
        p.fly();
        p.eat();
    }
}
class crow extends Bird // simple inheritance
{
    void colour()
    {
        System.out.println("i am white");
    }
    public static void main(String[] args)
    {
        crow c = new crow();
        c.fly();
        c.colour();
    }

    
}
