 
#include<iostream>
using namespace std;
class base{
    private:
    int A=10;
    public:
    int B=20;
    protected:
    int C=30;
    int getA() {
        return A;
    }
    int getB() {
        return B;
    }
    int getC() {
        return C;
    }
};
class publicclass: public base
{
    public:
    int getC()
    {
        return C;
    }
};
class protectedclass:protected base
{
    public:
    int getB()
    {
        return B;
    }
    int getC()
    {
        return C;
    }
};
class privatedclass: private base
{
     public:
    int getB()
    {
        return B;
    }
    int getC()
    {
        return C;
    }
};
int main()
{
    publicclass obj1;
    cout<<"....PUBLIC INHERITANCE...."<<endl;
    //cout << "Private = " << obj1.getX() << endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj1.getC() << endl;
    cout << "Public = " << obj1.B << endl;
    protectedclass obj2;
    cout<<"....PROTECTED INHERITANCE...."<<endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj2.getC() << endl;
    cout << "Public = " << obj2.getB() << endl;
    privatedclass obj3;
    cout<<"....PRIVATE INHERITANCE...."<<endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj3.getC() << endl;
    cout << "Public = " << obj3.getB() << endl;
    return 0;
}

