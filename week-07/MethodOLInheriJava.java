import java.util.*;
class parent {
    public void add(int a, int b) {
        System.out.println("sum of two numbers = "+(a+b));
    }
}

class derived extends parent {
    public void add(int a, int b,int c) {
        System.out.println("sum of three numbers = "+(a+b+c));
    }
    
    public static void main(String args[]) {
        derived obj = new derived();
        obj.add(41,45);
        obj.add(24,75,16);
    
    }
}


