import java.util.Scanner;
class EvenOdd
{
    void EvenOdd(int s) {
        if (s % 2 == 0)
            System.out.println("Number is even");
        else
            System.out.println("Number is odd");
    }
    public static void main(String[] args){
        System.out.println("Enter an integer:");
        Scanner input = new Scanner(System.in);
        int s = input.nextInt();
        EvenOdd obj = new EvenOdd();
        obj.EvenOdd(s);
    }
}
